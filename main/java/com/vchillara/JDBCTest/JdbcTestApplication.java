package com.vchillara.JDBCTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import com.vchillara.JDBCTest.model.Employee;
import com.vchillara.JDBCTest.model.EmployeeInsurance;
import com.vchillara.JDBCTest.service.EmployeeService;
import com.vchillara.JDBCTest.service.OrganizationService;

@SpringBootApplication
public class JdbcTestApplication {

	@Autowired
	EmployeeService employeeService;

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(JdbcTestApplication.class, args);
		
		OrganizationService orgService = context.getBean(OrganizationService.class);
		
		Employee emp= new Employee();
		emp.setEmpId("8564");
		emp.setEmpName("Prashanth Chillara");
		
		EmployeeInsurance ins = new EmployeeInsurance();
		ins.setEmpId("8564");
		ins.setInsuranceScheme("Premium");
		ins.setCoverageAmount(200000);
		
		orgService.leaveOrganization(emp, ins);

	}

}
