package com.vchillara.JDBCTest.service;

import com.vchillara.JDBCTest.model.EmployeeInsurance;

public interface EmployeeInsuranceService {

	public void registerInsurance(EmployeeInsurance empInsurance) ;
	public void deleteInsuranceById(String empId);
}
