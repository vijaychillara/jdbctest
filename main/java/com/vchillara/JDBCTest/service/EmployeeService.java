package com.vchillara.JDBCTest.service;

import java.util.List;

import com.vchillara.JDBCTest.model.Employee;

public interface EmployeeService {
	
	void insertEmployee(Employee emp);
	void insertEmployees(List<Employee> emps);
	void getAllEmployees();
	void getEmployeeById(String empId);
	public void deleteEmployeeById(String empid);

}
