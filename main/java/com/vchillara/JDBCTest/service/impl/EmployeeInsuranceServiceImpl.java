package com.vchillara.JDBCTest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vchillara.JDBCTest.dao.EmployeeInsuranceDao;
import com.vchillara.JDBCTest.model.EmployeeInsurance;
import com.vchillara.JDBCTest.service.EmployeeInsuranceService;

@Service
public class EmployeeInsuranceServiceImpl implements EmployeeInsuranceService {

	@Autowired
	EmployeeInsuranceDao insuranceDao;
	
	@Override
	public void registerInsurance(EmployeeInsurance empInsurance) {

		insuranceDao.registerInsurance(empInsurance);
	}

	@Override
	public void deleteInsuranceById(String empId) {
	
		insuranceDao.deleteInsuranceById(empId);
		
	}

}
