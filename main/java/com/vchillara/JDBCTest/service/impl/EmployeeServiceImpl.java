package com.vchillara.JDBCTest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vchillara.JDBCTest.dao.EmployeeDao;
import com.vchillara.JDBCTest.model.Employee;
import com.vchillara.JDBCTest.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	EmployeeDao empDao;
	

	public void insertEmployee(Employee emp) { 
		empDao.insertEmployee(emp);
	}


	public void insertEmployees(List<Employee> empList) { 
		empDao.insertEmployees(empList);
	}

	public void getAllEmployees() {
		List<Employee> employees = empDao.getAllEmployees();
		for(Employee emp:employees) { 
			System.out.println(emp.toString());
		}
		
	}
	

	public void getEmployeeById(String empId) {
		Employee employee = empDao.getEmployeeById(empId);
		System.out.println(employee);
	}
	
	public void deleteEmployeeById(String empid) { 
		empDao.deleteEmployeeById(empid);
	}

}
