package com.vchillara.JDBCTest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vchillara.JDBCTest.model.Employee;
import com.vchillara.JDBCTest.model.EmployeeInsurance;
import com.vchillara.JDBCTest.service.EmployeeInsuranceService;
import com.vchillara.JDBCTest.service.EmployeeService;
import com.vchillara.JDBCTest.service.OrganizationService;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	EmployeeService empService;

	@Autowired
	EmployeeInsuranceService empInsuranceService;

	@Transactional
	public void joinOrganization(Employee emp, EmployeeInsurance ins) {

		empService.insertEmployee(emp);
		
		empInsuranceService.registerInsurance(ins);
	}

	//@Transactional
	public void leaveOrganization(Employee emp, EmployeeInsurance ins) {
	
		empService.deleteEmployeeById(emp.getEmpId());
		if (emp.getEmpId().equals("8564")) {
			throw new RuntimeException("thowing exception to test transaction rollback");
		}
		
		empInsuranceService.deleteInsuranceById(emp.getEmpId());
	}

	
}
