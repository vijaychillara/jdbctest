package com.vchillara.JDBCTest.service;

import com.vchillara.JDBCTest.model.Employee;
import com.vchillara.JDBCTest.model.EmployeeInsurance;

public interface OrganizationService {

	public void joinOrganization(Employee emp, EmployeeInsurance ins);
	public void leaveOrganization(Employee emp, EmployeeInsurance ins);
	
}
