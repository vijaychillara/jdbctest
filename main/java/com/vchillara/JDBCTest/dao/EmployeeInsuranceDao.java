package com.vchillara.JDBCTest.dao;

import com.vchillara.JDBCTest.model.EmployeeInsurance;

public interface EmployeeInsuranceDao {

	void registerInsurance(EmployeeInsurance empInsurance);
	void deleteInsuranceById(String empId);
}
