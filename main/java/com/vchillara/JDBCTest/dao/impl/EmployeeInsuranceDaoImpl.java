package com.vchillara.JDBCTest.dao.impl;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.vchillara.JDBCTest.dao.EmployeeInsuranceDao;
import com.vchillara.JDBCTest.model.EmployeeInsurance;

@Repository
public class EmployeeInsuranceDaoImpl extends JdbcDaoSupport implements EmployeeInsuranceDao{

	@Autowired 
	DataSource dataSource;

	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}
	
	@Override
	public void registerInsurance(EmployeeInsurance empInsurance) {

		String sql = "INSERT INTO employee_insurance " +
				"(empId, insurance_scheme, coverage_amount) VALUES (?, ?, ?)" ;
		getJdbcTemplate().update(sql, new Object[]{
				empInsurance.getEmpId(), empInsurance.getInsuranceScheme(),empInsurance.getCoverageAmount()
		});

	}

	@Override
	public void deleteInsuranceById(String empId) {

		String sql = "DELETE FROM employee_insurance WHERE empId = ?";
		getJdbcTemplate().update(sql, new Object[] { empId });

	}

}
