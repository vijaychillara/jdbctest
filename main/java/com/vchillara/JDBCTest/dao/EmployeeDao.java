package com.vchillara.JDBCTest.dao;

import java.util.List;

import com.vchillara.JDBCTest.model.Employee;

public interface EmployeeDao {

	void insertEmployee(Employee emp);
	void insertEmployees(List<Employee> emps);
	List<Employee> getAllEmployees();
	Employee getEmployeeById(String empId);
	void deleteEmployeeById(String empid);

}
